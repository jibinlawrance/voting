package com.college.pojo;

public class CandidateResult {
    private String C_name;
    private String c_party;
    private String C_vote;

    public CandidateResult(String c_name, String c_party, String c_vote) {
        C_name = c_name;
        this.c_party = c_party;
        C_vote = c_vote;
    }

    public String getC_name() {
        return C_name;
    }

    public void setC_name(String c_name) {
        C_name = c_name;
    }

    public String getC_party() {
        return c_party;
    }

    public void setC_party(String c_party) {
        this.c_party = c_party;
    }

    public String getC_vote() {
        return C_vote;
    }

    public void setC_vote(String c_vote) {
        C_vote = c_vote;
    }
}
