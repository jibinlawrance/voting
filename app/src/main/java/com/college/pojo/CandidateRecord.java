package com.college.pojo;

public class CandidateRecord {

    public String getC_id() {
        return c_id;
    }

    public void setC_id(String c_id) {
        this.c_id = c_id;
    }

    String c_id;
    String c_name;
    String c_party;

    public CandidateRecord(String c_id, String c_name, String c_party) {
        this.c_id = c_id;
        this.c_name = c_name;
        this.c_party = c_party;
    }

    public String getC_name() {
        return c_name;
    }

    public void setC_name(String c_name) {
        this.c_name = c_name;
    }

    public String getC_party() {
        return c_party;
    }

    public void setC_party(String c_party) {
        this.c_party = c_party;
    }
}
