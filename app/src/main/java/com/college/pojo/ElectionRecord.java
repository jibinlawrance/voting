package com.college.pojo;

public class ElectionRecord {
    private String e_id;
    private String E_name;
    private String E_State;
    private String E_seat;
    private String E_date;

    public ElectionRecord(String e_id, String e_name, String e_State, String e_seat, String e_date) {
        this.e_id = e_id;
        E_name = e_name;
        E_State = e_State;
        E_seat = e_seat;
        E_date = e_date;
    }

    public String getE_id() {
        return e_id;
    }

    public void setE_id(String e_id) {
        this.e_id = e_id;
    }

    public String getE_name() {
        return E_name;
    }

    public void setE_name(String e_name) {
        E_name = e_name;
    }

    public String getE_State() {
        return E_State;
    }

    public void setE_State(String e_State) {
        E_State = e_State;
    }

    public String getE_seat() {
        return E_seat;
    }

    public void setE_seat(String e_seat) {
        E_seat = e_seat;
    }

    public String getE_date() {
        return E_date;
    }

    public void setE_date(String e_date) {
        E_date = e_date;
    }
}
