package com.college.pojo;

public class ShowResult {
    private String Candidate_name;
    private String c_email;
    private String c_party;
    private String c_phone;
    private String c_id;

    public ShowResult(String candidate_name, String c_email, String c_party, String c_phone, String c_id) {
        Candidate_name = candidate_name;
        this.c_email = c_email;
        this.c_party = c_party;
        this.c_phone = c_phone;
        this.c_id = c_id;
    }

    public String getCandidate_name() {
        return Candidate_name;
    }

    public void setCandidate_name(String candidate_name) {
        Candidate_name = candidate_name;
    }

    public String getC_email() {
        return c_email;
    }

    public void setC_email(String c_email) {
        this.c_email = c_email;
    }

    public String getC_party() {
        return c_party;
    }

    public void setC_party(String c_party) {
        this.c_party = c_party;
    }

    public String getC_phone() {
        return c_phone;
    }

    public void setC_phone(String c_phone) {
        this.c_phone = c_phone;
    }

    public String getC_id() {
        return c_id;
    }

    public void setC_id(String c_id) {
        this.c_id = c_id;
    }
}
