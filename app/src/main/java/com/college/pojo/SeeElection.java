package com.college.pojo;

public class SeeElection {
    String e_id;
    private String name;
    private String date;

    public SeeElection(String e_id, String name, String date) {
        this.e_id = e_id;
        this.name = name;
        this.date = date;
    }


    public String getE_id() {
        return e_id;
    }

    public void setE_id(String e_id) {
        this.e_id = e_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
