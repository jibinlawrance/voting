package com.college.util;

public class Keys {
    public static String HOME_PATH = "http://192.168.43.241/college/votingapp/admin/";
    public static String HOME_USER = "http://192.168.43.241/college/votingapp/user/";

    public static class URL
    {
        public static String admin_login = HOME_PATH +"a_login.php";
        public static String addElection = HOME_PATH +"add_election.php";
        public static String add_candidate = HOME_PATH +"add_candidate.php";
        public static String admin_get_election = HOME_PATH +"get_all_election.php";
        public static String admin_get_candidate = HOME_PATH +"get_all_candidate.php";
        public static String admin_candidate_result = HOME_PATH + "get_election_result.php";


        public static String u_See_Election = HOME_USER+"all_election.php";
        public static String u_See_Candidate = HOME_USER + "all_candidate.php";
        public static String u_login = HOME_USER + "u_login.php";
        public static String voting = HOME_USER + "u_voting.php";
    }
}
