package com.college.fragment;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.college.admin.AdminLoginActivity;
import com.college.admin.AdminMenuActivity;
import com.college.util.AppController;
import com.college.util.Keys;
import com.college.util.SharedPreference;
import com.college.votingsystem.R;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class AddElectionFragment extends Fragment {

    EditText edtname, edtstate, edtseat, edtDate;
    Button addElection;


    Calendar myCalender;
//    private int mYear, mMonth, mDay;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_election, container, false);
        edtDate = view.findViewById(R.id.eDate);
        edtname = view.findViewById(R.id.eName);
        edtstate = view.findViewById(R.id.eState);
        edtseat = view.findViewById(R.id.eSeat);
        myCalender=Calendar.getInstance();

        edtDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog.OnDateSetListener date=new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                        myCalender.set(Calendar.YEAR,year);
                        myCalender.set(Calendar.MONTH,monthOfYear);
                        myCalender.set(Calendar.DAY_OF_MONTH,dayOfMonth);
                        updateLabel();
                    }
                };
                new DatePickerDialog(getContext(),date,myCalender.get(Calendar.YEAR),
                        myCalender.get(Calendar.MONTH),myCalender.get(Calendar.DAY_OF_MONTH)).show();

            }
        });
        addElection = view.findViewById(R.id.eDetailAdd);
        addElection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(getContext(),"Record Insert Successfully",Toast.LENGTH_SHORT).show();
                String e_name = edtname.getText().toString().trim();
                String e_state = edtstate.getText().toString().trim();
                String e_seat = edtseat.getText().toString().trim();
                String e_Date = edtDate.getText().toString().trim();

                if (e_name.equals("")||e_state.equals("")||e_seat.equals("")||e_Date.equals(""))
                {
                    Toast.makeText(getContext(), "please fill all the details", Toast.LENGTH_SHORT).show();
                }else {
                    Election(e_name,e_state,e_seat,e_Date);
                }

            }
        });
        return view;

    }

    private void Election(final String e_name, final String e_state, final String e_seat, final String e_date) {
        StringRequest request = new StringRequest(Request.Method.POST, Keys.URL.addElection, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i("tag","login"+response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getString("success").equals("1"))
                    {
                       Toast.makeText(getContext(),"Election added Successfully",Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getContext(), AdminMenuActivity.class);
                        startActivity(intent);
                        clear();
                    }else {
                        Toast.makeText(getContext(), jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                }catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Toast.makeText(getContext(), "Technical problem arises", Toast.LENGTH_SHORT).show();
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("e_name",e_name);
                params.put("e_state",e_state);
                params.put("e_seat",e_seat);
                params.put("e_date",e_date);
                return params;
            }
        };
        AppController.getInstance().add(request);
    }

    private void clear() {
        edtname.setText("");
        edtstate.setText("");
        edtseat.setText("");
        edtDate.setText("");
    }


    private void updateLabel() {
        String myFormat="yyyy-MM-dd";
        SimpleDateFormat sdf=new SimpleDateFormat(myFormat,Locale.US);
        edtDate.setText(sdf.format(myCalender.getTime()));


    }

}
