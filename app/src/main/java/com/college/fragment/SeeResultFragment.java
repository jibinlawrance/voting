package com.college.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.college.adapter.ElectionResultAdapter;
import com.college.adapter.SeeElectionAdapter;
import com.college.pojo.ElectionResult;
import com.college.pojo.SeeElection;
import com.college.util.AppController;
import com.college.util.Keys;
import com.college.votingsystem.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class SeeResultFragment extends Fragment {
    RecyclerView recyclerView;
    ElectionResultAdapter adapter;
    ArrayList<ElectionResult> list;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_see_result,container,false);
        recyclerView=view.findViewById(R.id.seeResultRecycler);
        list=new ArrayList<>();
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
//        list.add(new ElectionResult("a","20 jan 2020"));
//
//        adapter=new ElectionResultAdapter(getContext(),list);
//        recyclerView.setAdapter(adapter);

        electionResult();
        return view;

    }

    private void electionResult() {
        StringRequest request = new StringRequest(Request.Method.POST, Keys.URL.admin_get_election, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    if (jsonObject.getString("success").equals("1"))
                    {
                        JSONArray jsonArray=jsonObject.getJSONArray("data");
                        for (int i=0;i<jsonArray.length();i++)
                        {
                            JSONObject jsonObject1=jsonArray.getJSONObject(i);
                            list.add(new ElectionResult(
                                    jsonObject1.getString("e_id"),
                                    jsonObject1.getString("e_name"),
                                    jsonObject1.getString("e_date")));
                        }
                        adapter=new ElectionResultAdapter(getContext(),list);
                        recyclerView.setAdapter(adapter);
                    }else {
                        Toast.makeText(getContext(), jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }

                }catch (JSONException e){
                    e.printStackTrace();

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        })
        {

        };
        AppController.getInstance().add(request);
    }

}
