package com.college.votingsystem;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import android.view.View;

import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.college.adapter.CandidateAdapter;

import com.college.pojo.CandidateRecord;

import com.college.util.AppController;
import com.college.util.Keys;
import com.college.util.RecyclerItemClickListener;
import com.college.util.SharedPreference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CandidateActivity extends AppCompatActivity {

    RecyclerView recyclerView;


    ArrayList<CandidateRecord> Clist;
    String e_id,u_id,u_phone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_candidate);
        recyclerView = findViewById(R.id.candidate_List);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        Clist = new ArrayList<>();

        final AlertDialog.Builder alertDialog=new AlertDialog.Builder(this);

        Intent i = getIntent();
        e_id = i.getStringExtra("e_id");
        u_id = SharedPreference.get("u_id");
        u_phone = SharedPreference.get("u_phone");
        Log.i("pri",u_phone);
        //Log.i("Pri","e_id = "+ e_id + "  c_id = "+c_id + "  u_phone = "+u_id );

        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getApplicationContext(), recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, final int position) {
                alertDialog.setMessage("Are you  sure you want to submit this Vote");
                alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //Toast.makeText(CandidateActivity.this, "Voting Sucessfully done ", Toast.LENGTH_SHORT).show();
                        Vote(e_id,Clist.get(position).getC_id(),u_id,u_phone);

                    }
                }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(CandidateActivity.this, "You clicked No", Toast.LENGTH_SHORT).show();
                    }
                }).show();
            }

            @Override
            public void onLongItemClick(View view, int position) {

            }
        }));

    }

    private void Vote(final String e_id, final String c_id, final String u_id, final String u_phone) {
        StringRequest request = new StringRequest(Request.Method.POST, Keys.URL.voting, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i("tag","login"+response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getString("success").equals("1"))
                    {
                        Toast.makeText(CandidateActivity.this,"Vote Added",Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(CandidateActivity.this, SubmitActivity.class);
                        startActivity(intent);
                        finish();

                    }else {
                        Toast.makeText(CandidateActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                }catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("e_id",e_id);
                params.put("c_id",c_id);
                params.put("u_id",u_id);
                params.put("u_phone",u_phone);
                return params;
            }
        };
        AppController.getInstance().add(request);
    }

    @Override
    protected void onStart() {
        super.onStart();
        alCandidate(e_id);
    }

    private void alCandidate(final String e_id) {
        StringRequest request=new StringRequest(Request.Method.POST, Keys.URL.u_See_Candidate, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i("tag",response);
                try{
                    JSONObject jsonObject=new JSONObject(response);
                    if (jsonObject.getString("success").equals("1")){
                        JSONArray jsonArray=jsonObject.getJSONArray("data");
                        for (int i=0;i<jsonArray.length();i++){
                            JSONObject jsonObject1=jsonArray.getJSONObject(i);
                            Clist.add(new CandidateRecord(
                                    jsonObject1.getString("c_id"),
                                    jsonObject1.getString("c_name"),
                                    jsonObject1.getString("c_party")));

                        }
                        CandidateAdapter adapter=new CandidateAdapter(Clist,getApplicationContext());
                        recyclerView.setAdapter(adapter);
                    }


                }catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        })
        {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("e_id",e_id);
                return params;
            }
        };

        AppController.getInstance().add(request);

    }

}
