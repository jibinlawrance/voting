package com.college.votingsystem;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.college.admin.AdminLoginActivity;
import com.college.admin.AdminMenuActivity;
import com.college.util.SharedPreference;

public class MenuActivity extends AppCompatActivity {

    Button bt_admin,bt_user;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        bt_admin =(Button)findViewById(R.id.AdminButton);
        bt_user = (Button)findViewById(R.id.UserButton);

        bt_admin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SharedPreference.contains("a_email") && SharedPreference.contains("a_password")){
                    Intent intent = new Intent(MenuActivity.this, AdminMenuActivity.class);
                    startActivity(intent);
                }else {
                    Intent intent = new Intent(MenuActivity.this, AdminLoginActivity.class);
                    startActivity(intent);
                }
            }
        });
        bt_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MenuActivity.this,LoginActivity.class);
                startActivity(intent);
//                if (SharedPreference.contains("u_phone")) {
//                    Intent intent = new Intent(MenuActivity.this, SelectionActivity.class);
//                    startActivity(intent);
//                }else{
//                    Intent intent = new Intent(MenuActivity.this, LoginActivity.class);
//                    startActivity(intent);
//                }
            }
        });
    }
}
