package com.college.votingsystem;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EdgeEffect;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.college.adapter.ElectionAdapter;
import com.college.adapter.SeeElectionAdapter;
import com.college.pojo.ElectionRecord;
import com.college.pojo.SeeElection;
import com.college.util.AppController;
import com.college.util.Keys;
import com.college.util.SharedPreference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Map;

public class SelectionActivity extends AppCompatActivity {

    RecyclerView electionList;
    ElectionAdapter electionAdapter;
    ArrayList<ElectionRecord> list;
    String u_phone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selection_);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        electionList = findViewById(R.id.SeeElectionList);
        LinearLayoutManager layoutManager=new LinearLayoutManager(getApplicationContext());
        electionList.setLayoutManager(layoutManager);
        electionList.setHasFixedSize(true);
        list=new ArrayList<>();

//        if (SharedPreference.contains("u_id") && SharedPreference.contains("u_phone")) {
//            SharedPreference.removeKey("u_id");
//            SharedPreference.removeKey("u_phone");
//
//        }

    }
    @Override
    protected void onStart() {
        super.onStart();
        allElection();
    }

    private void allElection() {
        list.clear();
        StringRequest request = new StringRequest(Request.Method.POST, Keys.URL.u_See_Election, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    if (jsonObject.getString("success").equals("1"))
                    {
                        JSONArray jsonArray=jsonObject.getJSONArray("data");
                        for (int i=0;i<jsonArray.length();i++)
                        {
                            JSONObject jsonObject1=jsonArray.getJSONObject(i);
                            list.add(new ElectionRecord(
                                    jsonObject1.getString("e_id"),
                                    jsonObject1.getString("e_name"),
                                    jsonObject1.getString("e_state"),
                                    jsonObject1.getString("e_seat"),
                                    jsonObject1.getString("e_date")));
                        }
                        electionAdapter = new ElectionAdapter(getApplicationContext(),list);
                        electionList.setAdapter(electionAdapter);
                    }else {
                        Toast.makeText(getApplicationContext(), jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }

                }catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        })
        {
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                return super.getParams();
//            }
        };
        AppController.getInstance().add(request);
    }
}
