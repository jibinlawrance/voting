package com.college.votingsystem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.college.admin.AdminLoginActivity;
import com.college.util.AppController;
import com.college.util.Keys;
import com.college.util.SharedPreference;

import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class LoginActivity extends AppCompatActivity {

    EditText edtPhone,editText_OTP;
    Button UBTLogin, otp_send;
    String u_phone,otp,u_otp;
    private static final int My_PERMISSION_SEND_REQUEST = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        edtPhone =findViewById(R.id.ETPhone);
        UBTLogin =findViewById(R.id.ULogin);
        otp_send =findViewById(R.id.otp);
        editText_OTP=findViewById(R.id.edit_otp);


        otp_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                u_phone = edtPhone.getText().toString().trim();
                if (u_phone.length()!=10){
                    edtPhone.setError("Phone must be 10 Digit");
                }

                if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.M){
                    if (ActivityCompat.checkSelfPermission(getApplicationContext(),Manifest.permission.SEND_SMS) +
                            ActivityCompat.checkSelfPermission(getApplicationContext(),Manifest.permission.READ_PHONE_STATE) +
                            ActivityCompat.checkSelfPermission(getApplicationContext(),Manifest.permission.READ_SMS)!=PackageManager.PERMISSION_GRANTED){
                        String[] permissions={Manifest.permission.SEND_SMS,Manifest.permission.READ_PHONE_STATE,Manifest.permission.READ_SMS};
                        requestPermissions(permissions,123);
                    }

                }
            }
        });

        UBTLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                u_otp=editText_OTP.getText().toString().trim();
                if (u_otp.equals("")){
                    Toast.makeText(LoginActivity.this, "please enter OTP", Toast.LENGTH_SHORT).show();
                }else if(u_otp.equals(otp)){
                    Toast.makeText(LoginActivity.this, "OTP matched", Toast.LENGTH_SHORT).show();
                    login(u_phone);
                }else{
                    Toast.makeText(getApplicationContext(), "PLease enter correct OTP", Toast.LENGTH_SHORT).show();
                }


            }
        });

    }

    private void login(final String u_phone) {
        StringRequest request = new StringRequest(Request.Method.POST, Keys.URL.u_login, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i("tag","login"+response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getString("success").equals("1"))
                    {
                        String u_id = jsonObject.getString("u_id");
                        Toast.makeText(LoginActivity.this,"Login Successfull",Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(LoginActivity.this, SelectionActivity.class);

                        SharedPreference.save("u_id",u_id);
                        SharedPreference.save("u_phone",u_phone);
//                        intent.putExtra("u_id",u_id);
//                        intent.putExtra("u_phone",u_phone);

                        startActivity(intent);
                        finish();
                    }
                    else {
                        Toast.makeText(LoginActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }

                }catch (Exception e)
                {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();

            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("u_phone",u_phone);
                return params;
            }
        };
        AppController.getInstance().add(request);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode==123){
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED){
                sendSms();
            }else{
                Toast.makeText(this, "you have to accept this permission", Toast.LENGTH_SHORT).show();
            }
        }else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        }
    }

    private void sendSms() {
        try
        {
            otp=new DecimalFormat("000000").format(new Random().nextInt(999999));

            SmsManager myManager=SmsManager.getDefault();
            myManager.sendTextMessage(u_phone,null,otp,null,null);
            Toast.makeText(LoginActivity.this,"OTP has Successfully Sent!",Toast.LENGTH_SHORT);
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
