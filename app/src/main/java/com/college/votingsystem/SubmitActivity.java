package com.college.votingsystem;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.college.util.SharedPreference;

public class SubmitActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_submit);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (SharedPreference.contains("u_id") && SharedPreference.contains("u_phone")) {
                    SharedPreference.removeKey("u_id");
                    SharedPreference.removeKey("u_phone");
                    Intent intent = new Intent(SubmitActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        },3000);
    }

    @Override
    public void onBackPressed() {

    }
}
