package com.college.admin;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DownloadManager;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.college.util.AppController;
import com.college.util.Keys;
import com.college.util.SharedPreference;
import com.college.votingsystem.R;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class AdminLoginActivity extends AppCompatActivity {

    EditText edtmail,editpass;
    Button loginbtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_login);
        edtmail=findViewById(R.id.editEmail);
        editpass=findViewById(R.id.editPass);

        loginbtn=findViewById(R.id.loginbtn);
        loginbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String a_email = edtmail.getText().toString().trim();
                String a_password = editpass.getText().toString().trim();

                if (a_email.equals("") || a_password.equals(""))
                {
                    Toast.makeText(AdminLoginActivity.this,"Please fill all the field",Toast.LENGTH_SHORT).show();
                }
                else
                {
                    a_login(a_email,a_password);
                }
//                Toast.makeText(AdminLoginActivity.this,"Login Successfull",Toast.LENGTH_SHORT).show();
//                Intent intent = new Intent(AdminLoginActivity.this,AdminMenuActivity.class);
//                startActivity(intent);
            }
        });
    }

    private void a_login(final String a_email, final String a_password) {
        StringRequest request=new StringRequest(Request.Method.POST, Keys.URL.admin_login, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i("tag","login"+response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getString("success").equals("1"))
                    {
                        //JSONObject jsonObject1 = jsonObject.getJSONObject("data");

                        Toast.makeText(AdminLoginActivity.this,"Login Successfully",Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(AdminLoginActivity.this,AdminMenuActivity.class);
                        SharedPreference.save("a_email",a_email);
                        SharedPreference.save("a_password",a_password);
                        startActivity(intent);
                        finish();
                    }else {
                        Toast.makeText(AdminLoginActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                }catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Toast.makeText(AdminLoginActivity.this, "Technical problem arises", Toast.LENGTH_SHORT).show();
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("a_email",a_email);
                params.put("a_password",a_password);
                return params;
            }
        };
        AppController.getInstance().add(request);
    }
}
