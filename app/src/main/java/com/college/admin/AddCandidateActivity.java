package com.college.admin;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.college.util.AppController;
import com.college.util.Keys;
import com.college.util.SharedPreference;
import com.college.votingsystem.R;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class AddCandidateActivity extends AppCompatActivity {

    EditText txtCname,txtCemail,txtparty,txtphone;
    Button btadd;

    String e_id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_candidate);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        txtCname=findViewById(R.id.CName);
        txtCemail=findViewById(R.id.CEmail);
        txtparty=findViewById(R.id.CParty);
        txtphone=findViewById(R.id.CPhone);

       Intent i = getIntent();
        e_id = i.getStringExtra("e_id");
        Log.i("TAG",e_id);
        //final String e_id=SharedPreference.get("e_id");

        btadd=findViewById(R.id.Cadd);
        btadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String c_name = txtCname.getText().toString().trim();
                String c_email = txtCemail.getText().toString().trim();
                String c_party = txtparty.getText().toString().trim();
                String c_phone = txtphone.getText().toString().trim();

                if (c_name.equals("")||c_email.equals("")||c_party.equals("")||c_phone.equals(""))
                {
                    Toast.makeText(AddCandidateActivity.this, "please fill all the details", Toast.LENGTH_SHORT).show();
                }else if (c_phone.length()!=10){
                    txtphone.setError("Phone-no Must be 10 Digit");

                }else{
                    candidate(c_name,c_email,c_party,c_phone,e_id);
                }
            }
        });
    }

    private void candidate(final String c_name, final String c_email, final String c_party, final String c_phone, final String e_id) {
        StringRequest request = new StringRequest(Request.Method.POST, Keys.URL.add_candidate, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //Log.i("tag","login"+response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getString("success").equals("1"))
                    {
                        //JSONObject jsonObject1=jsonObject.getJSONObject("data");
                        //String c_id = jsonObject.getString("c_id");
                        Toast.makeText(AddCandidateActivity.this,"Candidate added Successfully",Toast.LENGTH_SHORT).show();
                        //SharedPreference.save("c_id",c_id);
                        //Log.i("tag" , c_id);

                        finish();
                    }else {
                        Toast.makeText(AddCandidateActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                }catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Toast.makeText(AddCandidateActivity.this, "Technical problem arises", Toast.LENGTH_SHORT).show();
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("c_name",c_name);
                params.put("c_email",c_email);
                params.put("c_party",c_party);
                params.put("c_phone",c_phone);
                params.put("e_id",e_id);
                return params;
            }
        };
        AppController.getInstance().add(request);
    }


    private void clear() {
        txtCname.setText("");
        txtCemail.setText("");
        txtparty.setText("");
        txtphone.setText("");
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
            default:
                return super.onOptionsItemSelected(item);
        }

    }
}
