package com.college.admin;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;

import android.util.Log;
import android.view.MenuItem;
import android.widget.RelativeLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.college.adapter.CandidateResultAdapter;
import com.college.adapter.ShowCandidateAdapter;
import com.college.pojo.CandidateResult;

import com.college.pojo.ShowResult;
import com.college.util.AppController;
import com.college.util.Keys;
import com.college.util.SharedPreference;
import com.college.votingsystem.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CandidateResultActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    CandidateResultAdapter adapter;
    ArrayList<CandidateResult> list;

    String  e_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_candidate_result);
        recyclerView = findViewById(R.id.recycler_result);
        getSupportActionBar().setTitle("Result");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        list = new ArrayList<>();
        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        //Intent i = getIntent();
        //e_id = i.getStringExtra("e_id");
        e_id =SharedPreference.get("e_id");
        Log.i("Tag",e_id);

        /*list.add(new CandidateResult("ABC", "20"));
        list.add(new CandidateResult("xyz", "30"));
        list.add(new CandidateResult("pqr", "60"));
        list.add(new CandidateResult("lmn", "30"));
        adapter = new CandidateResultAdapter(getApplicationContext(), list);
        recyclerView.setAdapter(adapter);*/


    }

    @Override
    protected void onStart() {
        super.onStart();
        C_Result(e_id);
    }

    private void C_Result(final String e_id) {
        StringRequest request = new StringRequest(Request.Method.POST, Keys.URL.admin_candidate_result, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if(jsonObject.getString("success").equals("1"))
                    {
                        JSONArray jsonArray=jsonObject.getJSONArray("data");
                        for (int i=0;i<jsonArray.length();i++){
                            JSONObject jsonObject1=jsonArray.getJSONObject(i);
                            list.add(new CandidateResult(jsonObject1.getString("c_name"),
                                    jsonObject1.getString("c_party"),
                                    jsonObject1.getString("count")));
                        }
                        adapter = new CandidateResultAdapter(getApplicationContext(), list);
                        recyclerView.setAdapter(adapter);

                    }
                }catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("e_id",e_id);
                return params;
            }
        };
        AppController.getInstance().add(request);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
                case android.R.id.home:
                    onBackPressed();
                default:
                    return super.onOptionsItemSelected(item);
        }

    }
}
