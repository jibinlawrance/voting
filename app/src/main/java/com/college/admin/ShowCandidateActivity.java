package com.college.admin;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.college.adapter.ShowCandidateAdapter;
import com.college.pojo.ShowResult;
import com.college.util.AppController;
import com.college.util.Keys;
import com.college.util.SharedPreference;
import com.college.votingsystem.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ShowCandidateActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    ShowCandidateAdapter adapter;
    ArrayList<ShowResult> list;

    String e_id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_candidate);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        recyclerView = findViewById(R.id.RecyclerShowList);
        list=new ArrayList<>();
        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        //String e_id= SharedPreference.get("e_id");

        Intent i = getIntent();
        e_id = i.getStringExtra("e_id");

    }

    @Override
    protected void onStart() {
        super.onStart();
        CandidateList(e_id);
    }

    private void CandidateList(final String e_id) {
        list.clear();
        StringRequest request=new StringRequest(Request.Method.POST, Keys.URL.admin_get_candidate, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    if (jsonObject.getString("success").equals("1")){
                        JSONArray jsonArray=jsonObject.getJSONArray("data");
                        for (int i=0;i<jsonArray.length();i++){
                            JSONObject jsonObject1=jsonArray.getJSONObject(i);
                            list.add(new ShowResult(jsonObject1.getString("c_name"),
                                    jsonObject1.getString("c_email"),
                                    jsonObject1.getString("c_party"),
                                    jsonObject1.getString("c_phone"),
                                    jsonObject1.getString("c_id")));

                        }
                        adapter=new ShowCandidateAdapter(getApplicationContext(),list);
                        recyclerView.setAdapter(adapter);
                    }else {
                        Toast.makeText(ShowCandidateActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }

                }catch (JSONException e){
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();

            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("e_id",e_id);
                return params;
            }
        };
        AppController.getInstance().add(request);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.menu,menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_add:
                Intent intent = new Intent(ShowCandidateActivity.this, AddCandidateActivity.class);
                intent.putExtra("e_id", e_id);
                startActivity(intent);
            break;
            case android.R.id.home:
                finish();
                onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }


    

}
