package com.college.admin;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.college.adapter.ViewPagerAdapter;
import com.college.fragment.AddElectionFragment;
import com.college.fragment.SeeElectionFragment;
import com.college.fragment.SeeResultFragment;
import com.college.util.SharedPreference;
import com.college.votingsystem.R;
import com.google.android.material.tabs.TabLayout;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;




public class AdminMenuActivity extends AppCompatActivity {

    private androidx.appcompat.widget.Toolbar toolbar;
    private TabLayout tabLayout;

    private ViewPager viewPager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_menu);
        viewPager = findViewById(R.id.view_pager);
        tabLayout = findViewById(R.id.tabs);
        toolbar =  findViewById(R.id.toolBar);
        tabLayout=(TabLayout)findViewById(R.id.tabs);
        viewPager =(ViewPager)findViewById(R.id.view_pager);
        setupViewpager(viewPager);
        setSupportActionBar(toolbar);
        tabLayout.setupWithViewPager(viewPager);

    }
    private void setupViewpager(ViewPager viewPager)
    {
            ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
            viewPagerAdapter.addFragment(new AddElectionFragment(),"Add Election");
            viewPagerAdapter.addFragment(new SeeElectionFragment(),"See Election");
            viewPagerAdapter.addFragment(new SeeResultFragment(),"See Result");
            viewPager.setAdapter(viewPagerAdapter);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.logout,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.logout:
                if (SharedPreference.contains("a_email") && SharedPreference.contains("a_password")) {
                    SharedPreference.removeKey("a_email");
                    SharedPreference.removeKey("a_password");
                    Intent intent = new Intent(AdminMenuActivity.this, AdminLoginActivity.class);
                    startActivity(intent);
                    finish();
                }
                break;
        }

        return super.onOptionsItemSelected(item);
    }

}