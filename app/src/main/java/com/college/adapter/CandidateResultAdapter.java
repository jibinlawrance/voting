package com.college.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.college.pojo.CandidateResult;
import com.college.pojo.ShowResult;
import com.college.votingsystem.R;

import java.util.ArrayList;

public class CandidateResultAdapter extends RecyclerView.Adapter<CandidateResultAdapter.ViewHolder> {

    private Context context;
    private ArrayList<CandidateResult> list;

    public CandidateResultAdapter(Context context, ArrayList<CandidateResult> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public CandidateResultAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.admin_candidate_result_layout,parent,false);
        view.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT,RecyclerView.LayoutParams.WRAP_CONTENT));
        return new CandidateResultAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CandidateResultAdapter.ViewHolder holder, int position) {
        CandidateResult showResult=list.get(position);
        holder.txtname.setText("Candidate Name: "+ showResult.getC_name());
        holder.txtparty.setText("Candidate Party: "+ showResult.getC_party());
        holder.txtVote.setText("vote: "+showResult.getC_vote());

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtname,txtparty,txtVote;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txtname=itemView.findViewById(R.id.txt_c_name);
            txtparty=itemView.findViewById(R.id.txt_c_par);
            txtVote=itemView.findViewById(R.id.txt_C_vote);
        }
    }
}
