package com.college.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ShareActionProvider;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.college.admin.CandidateResultActivity;
import com.college.pojo.ElectionResult;
import com.college.util.SharedPreference;
import com.college.votingsystem.R;

import java.util.ArrayList;

public class ElectionResultAdapter extends RecyclerView.Adapter<ElectionResultAdapter.ViewHolder> {
    private Context context;
    private ArrayList<ElectionResult> list;

    public ElectionResultAdapter(Context context, ArrayList<ElectionResult> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ElectionResultAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.admin_show_elction_list_layout,parent,false);
        view.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT,RecyclerView.LayoutParams.WRAP_CONTENT));
        return new ElectionResultAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ElectionResultAdapter.ViewHolder holder, int position) {
        final ElectionResult result = list.get(position);
        holder.tname.setText("Election : "+result.getE_name());
        holder.tdate.setText("Election Date : "+result.getDate());

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, CandidateResultActivity.class);
                context.startActivity(intent);
                intent.putExtra("e_id",result.getE_id());
                SharedPreference.save("e_id",result.getE_id());
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tname,tdate;
        CardView cardView;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.tname=itemView.findViewById(R.id.txtname);
            this.tdate=itemView.findViewById(R.id.txtdate);
            cardView=itemView.findViewById(R.id.showListCard);
        }
    }
}
