package com.college.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.college.admin.ShowCandidateActivity;
import com.college.pojo.SeeElection;
import com.college.util.SharedPreference;
import com.college.votingsystem.R;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SeeElectionAdapter extends RecyclerView.Adapter<SeeElectionAdapter.ViewHolder>{

    private Context context;
    private ArrayList<SeeElection> list;


    public SeeElectionAdapter(Context context, ArrayList<SeeElection> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public SeeElectionAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.admin_see_election_custom_layout,parent,false);
        view.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT,RecyclerView.LayoutParams.WRAP_CONTENT));
        return new SeeElectionAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SeeElectionAdapter.ViewHolder holder, int position) {
        final SeeElection election = list.get(position);
        holder.txtElection.setText("Name: "+election.getName());
        holder.txtDate.setText("Date: "+election.getDate());





        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ShowCandidateActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                SharedPreference.save("e_id",election.getE_id());
                intent.putExtra("e_id", election.getE_id());
                context.startActivity(intent);



                //Collections.sort(list,Collections.<SeeElection>reverseOrder());
            }
        });
    }

    @Override
    public int getItemCount() {

        return list.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView txtElection,txtDate;
        CardView cardView;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.txtElection=itemView.findViewById(R.id.text_name);
            this.txtDate = itemView.findViewById(R.id.text_date);
            this.cardView = itemView.findViewById(R.id.SeeElectionCard);
        }
    }
}
