package com.college.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.college.pojo.CandidateRecord;
import com.college.votingsystem.LoginActivity;
import com.college.votingsystem.R;

import java.util.ArrayList;

public class CandidateAdapter extends RecyclerView.Adapter<CandidateAdapter.CandidateHolder> {


    private ArrayList<CandidateRecord> candidateRecords;
    private Context context;

    public CandidateAdapter(ArrayList<CandidateRecord> candidateRecords, Context context) {
        this.candidateRecords = candidateRecords;
        this.context = context;
    }

    @NonNull
    @Override
    public CandidateHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.candidate_custom_layout,parent,false);
        view.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT,RecyclerView.LayoutParams.WRAP_CONTENT));
        return new  CandidateHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CandidateHolder holder, int position) {
        CandidateRecord record = candidateRecords.get(position);

        holder.txtCname.setText(record.getC_name());
        holder.txtCparty.setText(record.getC_party());

    }

    @Override
    public int getItemCount() {
        return candidateRecords.size();
    }

    public class CandidateHolder extends RecyclerView.ViewHolder
    {

        CardView cView;
        TextView txtCname,txtCparty;
        public CandidateHolder(@NonNull View itemView) {
            super(itemView);
            cView = (CardView)itemView.findViewById(R.id.candidateCard);
            txtCname =(TextView)itemView.findViewById(R.id.candName);
            txtCparty = (TextView)itemView.findViewById(R.id.candParty);
        }
    }
}
