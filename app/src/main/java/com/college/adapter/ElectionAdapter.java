package com.college.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.college.pojo.ElectionRecord;
import com.college.util.SharedPreference;
import com.college.votingsystem.CandidateActivity;
import com.college.votingsystem.R;

import java.util.ArrayList;

public class ElectionAdapter extends RecyclerView.Adapter<ElectionAdapter.ElectionHolder> {

    private Context context;
    private ArrayList<ElectionRecord> list;

    public ElectionAdapter(Context context, ArrayList<ElectionRecord> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ElectionHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.election_custom_layout,parent,false);
        view.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT,RecyclerView.LayoutParams.WRAP_CONTENT));

        return new ElectionAdapter.ElectionHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ElectionHolder holder, int position) {
        final ElectionRecord record=list.get(position);
        holder.txtName.setText(record.getE_name());
        holder.txtState.setText(record.getE_State());
        holder.txtseat.setText(record.getE_seat());
        holder.txtDate.setText(record.getE_date());

        holder.CardElection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, CandidateActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                SharedPreference.save("e_id",record.getE_id());
                intent.putExtra("e_id", record.getE_id());
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {

        return list.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class ElectionHolder extends RecyclerView.ViewHolder{
        TextView txtName,txtState,txtDate,txtseat;
        CardView CardElection;
        public ElectionHolder(@NonNull View itemView) {
            super(itemView);
            txtName=(TextView)itemView.findViewById(R.id.Ele_name);
            txtState=(TextView)itemView.findViewById(R.id.Ele_StateName);
            txtDate=(TextView)itemView.findViewById(R.id.Ele_date);
            txtseat=(TextView)itemView.findViewById(R.id.Ele_Seats);
            CardElection=(CardView)itemView.findViewById(R.id.ElctionCard);
        }
    }
}
