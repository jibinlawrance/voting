package com.college.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.college.pojo.ShowResult;
import com.college.util.SharedPreference;
import com.college.votingsystem.R;

import java.util.ArrayList;

public class ShowCandidateAdapter extends RecyclerView.Adapter<ShowCandidateAdapter.ViewHolder> {

    private Context context;
    private ArrayList<ShowResult> list;

    public ShowCandidateAdapter(Context context, ArrayList<ShowResult> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ShowCandidateAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.admin_candidate_list_layout,parent,false);
        view.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT,RecyclerView.LayoutParams.WRAP_CONTENT));
        return new ShowCandidateAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ShowCandidateAdapter.ViewHolder holder, int position) {
        ShowResult showResult=list.get(position);
        holder.txtname.setText("Candidate Name: "+ showResult.getCandidate_name());
        holder.txtemail.setText("Email: "+showResult.getC_email());
        holder.txtparty.setText("Party: "+showResult.getC_party());
        holder.txtphone.setText("Phone: "+showResult.getC_phone());
        SharedPreference.save("c_id",showResult.getC_id());


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtname,txtemail,txtparty,txtphone;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txtname=itemView.findViewById(R.id.txtCName);
            txtemail=itemView.findViewById(R.id.txtemail);
            txtparty=itemView.findViewById(R.id.txtparty);
            txtphone=itemView.findViewById(R.id.txtphone);
        }
    }
}
